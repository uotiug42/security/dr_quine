global main
extern printf

; source code is saved in the data section
section .data
	code db "global main%1$cextern printf%1$c%1$c; source code is saved in the data section%1$csection .data%1$c%2$ccode db %3$c%4$s%3$c, 0%1$c%1$csection .text%1$cmain:%1$c%2$csub rsp, 8%1$c%2$cmov rdi, code%1$c%2$cmov rsi, 10%1$c%2$cmov rdx, 9%1$c%2$cmov rcx, 34%1$c%2$cmov r8, code%1$c%2$ccall printf%2$c%2$c; calling printf%1$c%2$cadd rsp, 8%1$c%2$ccall return%1$c%1$creturn:%1$c%2$cret%1$c", 0

section .text
main:
	sub rsp, 8
	mov rdi, code
	mov rsi, 10
	mov rdx, 9
	mov rcx, 34
	mov r8, code
	call printf		; calling printf
	add rsp, 8
	call return

return:
	ret
