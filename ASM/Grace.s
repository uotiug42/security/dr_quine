%define CODE code db "%2$cdefine CODE code db %1$c%3$s%1$c, 0%4$c%2$cdefine FILE file db %1$cGrace_kid.s%1$c, 0%4$c%2$cdefine START main%4$c%4$c; here is the macro%4$c%2$cmacro run_grace 0%4$cglobal START%4$cextern fprintf%4$cextern fopen%4$cextern fclose%4$c%4$csection .data%4$c%5$cCODE%4$c%5$cFILE%4$c%5$cmode db %1$cw%1$c, 0%4$c%4$csection .text%4$cSTART:%4$c%5$csub rsp, 8%4$c%5$cmov rdi, file%4$c%5$cmov rsi, mode%4$c%5$ccall fopen%4$c%5$cmov r15, rax%4$c%5$cmov rdi, r15%4$c%5$cmov rsi, code%4$c%5$cmov rdx, 34%4$c%5$cmov rcx, 37%4$c%5$cmov r8, code%4$c%5$cmov r9, 10%4$c%5$cmov r10, 9%4$c%5$cadd rsp, 8%4$c%5$cpush r10%4$c%5$ccall fprintf%4$c%5$cpop r10%4$c%5$csub rsp, 8%4$c%5$cmov rdi, r15%4$c%5$ccall fclose%4$c%5$cadd rsp, 8%4$c%5$cret%4$c%2$cendmacro%4$c%4$crun_grace%4$c", 0
%define FILE file db "Grace_kid.s", 0
%define START main

; here is the macro
%macro run_grace 0
global START
extern fprintf
extern fopen
extern fclose

section .data
	CODE
	FILE
	mode db "w", 0

section .text
START:
	sub rsp, 8
	mov rdi, file
	mov rsi, mode
	call fopen
	mov r15, rax
	mov rdi, r15
	mov rsi, code
	mov rdx, 34
	mov rcx, 37
	mov r8, code
	mov r9, 10
	mov r10, 9
	add rsp, 8
	push r10
	call fprintf
	pop r10
	sub rsp, 8
	mov rdi, r15
	call fclose
	add rsp, 8
	ret
%endmacro

run_grace
