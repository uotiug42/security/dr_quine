%define CODE code db "%2$cdefine CODE code db %1$c%3$s%1$c, 0%4$c%4$cglobal main%4$cextern fprintf%4$cextern fopen%4$cextern fclose%4$cextern strdup%4$cextern free%4$cextern system%4$cextern access%4$c%4$csection .data%4$c%5$cCODE%4$c%5$ccheck db %1$cSully_5.s%1$c, 0%4$c%5$cfile db %1$cSully_x.s%1$c, 0%4$c%5$cmode db %1$cw%1$c, 0%4$c%5$ccmd db %1$cnasm -f elf64 Sully_x.s -o Sully_x.o && cc Sully_x.o -o Sully_x && rm Sully_x.o; ./Sully_x%1$c, 0%4$c%4$csection .text%4$cmain:%4$c%5$csub rsp, 8%4$c%5$cmov r14b, %6$d%4$c%5$ccmp r14b, 1%4$c%5$cjl ret%4$c%5$cmov rdi, check%4$c%5$cmov rsi, 0x04%4$c%5$ccall access%4$c%5$ccmp rax, 0%4$c%5$cjne nodecr%4$c%5$cdec r14b%4$cnodecr:%4$c%5$cmov rdi, file%4$c%5$ccall strdup%4$c%5$cmov r13b, r14b%4$c%5$cadd r13b, '0'%4$c%5$cmov [rax + 6], byte r13b%4$c%5$cmov rdi, rax%4$c%5$cmov r12, rax%4$c%5$cmov rsi, mode%4$c%5$ccall fopen%4$c%5$cmov r15, rax%4$c%5$cmov rdi, r15%4$c%5$cmov rsi, code%4$c%5$cmov rdx, 34%4$c%5$cmov rcx, 37%4$c%5$cmov r8, code%4$c%5$cmov r9, 10%4$c%5$cmov r10, 9%4$c%5$cmovzx r14, r14b%4$c%5$cpush r14%4$c%5$cpush r10%4$c%5$ccall fprintf%4$c%5$cpop r10%4$c%5$cpop r14%4$c%5$cmov rdi, r15%4$c%5$ccall fclose%4$c%5$cmov rdi, r12%4$c%5$ccall free%4$c%5$cmov rdi, cmd%4$c%5$ccall strdup%4$c%5$cmov [rax + 20], byte r13b%4$c%5$cmov [rax + 33], byte r13b%4$c%5$cmov [rax + 49], byte r13b%4$c%5$cmov [rax + 62], byte r13b%4$c%5$cmov [rax + 76], byte r13b%4$c%5$cmov [rax + 89], byte r13b%4$c%5$cmov rdi, rax%4$c%5$cmov r15, rax%4$c%5$ccall system%4$c%5$cmov rdi, r15%4$c%5$ccall free%4$cret:%4$c%5$cadd rsp, 8%4$c%5$cret%4$c", 0

global main
extern fprintf
extern fopen
extern fclose
extern strdup
extern free
extern system
extern access

section .data
	CODE
	check db "Sully_5.s", 0
	file db "Sully_x.s", 0
	mode db "w", 0
	cmd db "nasm -f elf64 Sully_x.s -o Sully_x.o && cc Sully_x.o -o Sully_x && rm Sully_x.o; ./Sully_x", 0

section .text
main:
	sub rsp, 8
	mov r14b, 5
	cmp r14b, 1
	jl ret
	mov rdi, check
	mov rsi, 0x04
	call access
	cmp rax, 0
	jne nodecr
	dec r14b
nodecr:
	mov rdi, file
	call strdup
	mov r13b, r14b
	add r13b, '0'
	mov [rax + 6], byte r13b
	mov rdi, rax
	mov r12, rax
	mov rsi, mode
	call fopen
	mov r15, rax
	mov rdi, r15
	mov rsi, code
	mov rdx, 34
	mov rcx, 37
	mov r8, code
	mov r9, 10
	mov r10, 9
	movzx r14, r14b
	push r14
	push r10
	call fprintf
	pop r10
	pop r14
	mov rdi, r15
	call fclose
	mov rdi, r12
	call free
	mov rdi, cmd
	call strdup
	mov [rax + 20], byte r13b
	mov [rax + 33], byte r13b
	mov [rax + 49], byte r13b
	mov [rax + 62], byte r13b
	mov [rax + 76], byte r13b
	mov [rax + 89], byte r13b
	mov rdi, rax
	mov r15, rax
	call system
	mov rdi, r15
	call free
ret:
	add rsp, 8
	ret
