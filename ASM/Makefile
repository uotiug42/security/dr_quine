# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: gbrunet <gbrunet@student.42.fr>            +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2024/04/17 17:11:54 by gbrunet           #+#    #+#              #
#    Updated: 2024/04/30 13:43:52 by gbrunet          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

_BLACK = \033[0;30
_BLACK = \033[0;30m
_RED = \033[0;31m
_GREEN = \033[0;32m
_BLUE = \033[0;34m
_YELLOW = \033[0;33m
_PURPLE = \033[0;35m
_CYAN = \033[0;36m
_WHITE = \033[0;37m

_BOLD = \e[1m
_THIN = \e[2m

_END = \033[0m

ASM = nasm

CC = cc

CFLAGS = -f elf64

OBJ_DIR = objects/

SRC_FILES = Colleen Grace Sully

SRC = $(addsuffix .s, $(SRC_FILES))

OBJ = $(addprefix $(OBJ_DIR), $(addsuffix .o, $(SRC_FILES)))

COMPTEUR = 0

$(OBJ_DIR)%.o: %.s
	$(eval COMPTEUR=$(shell echo $$(($(COMPTEUR)+1))))
	@mkdir -p $(OBJ_DIR)
	@if test $(COMPTEUR) -eq 1;then \
		printf "$(_YELLOW)Compiling binary files...$(_END)\n";fi
	@printf "$(_CYAN)Binary $(COMPTEUR): $@$(_END)\n"
	@$(ASM) $(CFLAGS) $(INCLUDE) $< -o $@

.PHONY : all clean fclean re bonus norme

all : Colleen Grace Sully

Colleen : $(OBJ_DIR)Colleen.o
	@make info --no-print-directory
	@$(CC) $(OBJ_DIR)Colleen.o -o Colleen
	@echo "$(_GREEN)Colleen created$(_END)"

Grace : $(OBJ_DIR)Grace.o
	@make info --no-print-directory
	@$(CC) $(OBJ_DIR)Grace.o -o Grace
	@echo "$(_GREEN)Grace created$(_END)"

Sully : $(OBJ_DIR)Sully.o
	@make info --no-print-directory
	@$(CC) $(OBJ_DIR)Sully.o -o Sully
	@echo "$(_GREEN)Sully created$(_END)"

clean :
	@echo "$(_YELLOW)Colleen, Grace, Sully: Clean...$(_END)"
	@$(RM) -rf $(OBJ_DIR)
	@echo "$(_GREEN)Colleen, Grace, Sully: Binaries deleted...$(_END)"

fclean :
	@echo "$(_YELLOW)Colleen, Grace, Sully: Full clean...$(_END)"
	@$(RM) -rf $(OBJ_DIR)
	@echo "$(_GREEN)Colleen, Grace, Sully: Binaries deleted...$(_END)"
	@$(RM) Colleen Grace Sully
	@echo "$(_GREEN)Colleen, Grace, Sully deleted...$(_END)"

sclean :
	@make fclean --no-print-directory
	@$(RM) -rf Sully_0 Sully_0.s Sully_1 Sully_1.s Sully_2 Sully_2.s Sully_3 \
		Sully_3.s Sully_4 Sully_4.s Sully_5 Sully_5.s Grace_kid.s
	@echo "$(_GREEN)Output files deleted...$(_END)"

re : 
	@make fclean --no-print-directory
	@make all --no-print-directory

info :
	@printf "\t$(_PURPLE)╭──────────────────────────────────────╮"
	@printf "\n\t│$(_END)  👾  $(_CYAN)$(_THIN)Coded by $(_END)$(_CYAN)"
	@printf "$(_BOLD)guillaume brunet$(_END)$(_PURPLE)       │\n"
	@printf "\t│$(_END)  💬  $(_RED)$(_BOLD)Do not copy$(_END)$(_RED)$(_THIN), "
	@printf "$(_END)$(_RED)this is useless...$(_END) $(_PURPLE)│\n"
	@printf "\t╰──────────────────────────────────────╯\n$(_END)"

