#!/usr/bin/python
import sys
import os

# python does not support defines or macros
CODE = "#!/usr/bin/python%cimport sys%cimport os%c%c# python does not support defines or macros%cCODE = %c%s%c%cFILENAME = %c%s%c%cSTDOUT = sys.stdout%c%cdef grace():%c%cf = open(FILENAME, %cw%c, encoding=%cutf-8%c)%c%csys.stdout = f%c%cprint(CODE%%(chr(10), chr(10), chr(10), chr(10), chr(10), chr(34), CODE, chr(34), chr(10), chr(34), FILENAME, chr(34), chr(10), chr(10), chr(10), chr(10), chr(9), chr(34), chr(34), chr(34), chr(34), chr(10), chr(9), chr(10), chr(9), chr(10), chr(9), chr(10), chr(9), chr(10), chr(9), chr(10), chr(10), chr(10)))%c%csys.stdout = STDOUT%c%cos.chmod(FILENAME, 0o755)%c%cf.close()%c%cgrace()%c"
FILENAME = "Grace_kid.py"
STDOUT = sys.stdout

def grace():
	f = open(FILENAME, "w", encoding="utf-8")
	sys.stdout = f
	print(CODE%(chr(10), chr(10), chr(10), chr(10), chr(10), chr(34), CODE, chr(34), chr(10), chr(34), FILENAME, chr(34), chr(10), chr(10), chr(10), chr(10), chr(9), chr(34), chr(34), chr(34), chr(34), chr(10), chr(9), chr(10), chr(9), chr(10), chr(9), chr(10), chr(9), chr(10), chr(9), chr(10), chr(10), chr(10)))
	sys.stdout = STDOUT
	os.chmod(FILENAME, 0o755)
	f.close()

grace()

