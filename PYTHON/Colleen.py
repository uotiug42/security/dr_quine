#!/usr/bin/python
# the first comment
def code():
	str = "#!/usr/bin/python%c# the first comment%cdef code():%c%cstr = %c%s%c%c%cprint(str%%(chr(10), chr(10), chr(10), chr(9), chr(34), str, chr(34), chr(10), chr(9), chr(10), chr(10), chr(10), chr(9), chr(10), chr(9), chr(10), chr(10), chr(34), chr(34), chr(10), chr(9), chr(10)))%c%cdef main():%c%c# the main function%c%ccode()%c%cif __name__ == %c__main__%c:%c%cmain()%c"
	print(str%(chr(10), chr(10), chr(10), chr(9), chr(34), str, chr(34), chr(10), chr(9), chr(10), chr(10), chr(10), chr(9), chr(10), chr(9), chr(10), chr(10), chr(34), chr(34), chr(10), chr(9), chr(10)))

def main():
	# the main function
	code()

if __name__ == "__main__":
	main()

