#!/usr/bin/python
import os
import sys

CODE = "#!/usr/bin/python%cimport os%cimport sys%c%cCODE = %c%s%c%cI = %d%cSTDOUT = sys.stdout%c%cdef Sully():%c%ci = I%c%cif I < 1:%c%c%cquit()%c%cif os.path.isfile(%cSully_5.py%c):%c%c%ci = I - 1%c%cname = f'Sully_{i}.py'%c%cf = open(name, %cw%c, encoding=%cutf-8%c)%c%csys.stdout = f%c%cprint(CODE%%(chr(10), chr(10), chr(10), chr(10), chr(34), CODE, chr(34), chr(10), i, chr(10), chr(10), chr(10), chr(10), chr(9), chr(10), chr(9), chr(10), chr(9), chr(9), chr(10), chr(9), chr(34), chr(34), chr(10), chr(9), chr(9), chr(10), chr(9), chr(10), chr(9), chr(34), chr(34), chr(34), chr(34), chr(10), chr(9), chr(10), chr(9), chr(10), chr(9), chr(10), chr(9), chr(10), chr(9), chr(10), chr(9), chr(10), chr(10), chr(10)))%c%csys.stdout = STDOUT%c%cos.chmod(name, 0o755)%c%cf.close()%c%cos.system(f'python {name}')%c%cSully()%c"
I = 5
STDOUT = sys.stdout

def Sully():
	i = I
	if I < 1:
		quit()
	if os.path.isfile("Sully_5.py"):
		i = I - 1
	name = f'Sully_{i}.py'
	f = open(name, "w", encoding="utf-8")
	sys.stdout = f
	print(CODE%(chr(10), chr(10), chr(10), chr(10), chr(34), CODE, chr(34), chr(10), i, chr(10), chr(10), chr(10), chr(10), chr(9), chr(10), chr(9), chr(10), chr(9), chr(9), chr(10), chr(9), chr(34), chr(34), chr(10), chr(9), chr(9), chr(10), chr(9), chr(10), chr(9), chr(34), chr(34), chr(34), chr(34), chr(10), chr(9), chr(10), chr(9), chr(10), chr(9), chr(10), chr(9), chr(10), chr(9), chr(10), chr(9), chr(10), chr(10), chr(10)))
	sys.stdout = STDOUT
	os.chmod(name, 0o755)
	f.close()
	os.system(f'python {name}')

Sully()

